//คุมเรื่องการ login
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
        id: 1,
        email: 'mana123@gmail.com',
        password: 'Pas1234',
        fullname: 'มานา งามขำ',
        gender: 'male',
        roles: ['user']
    })

  return { currentUser }
})
